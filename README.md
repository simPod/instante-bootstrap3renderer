Instante/Bootstap3Renderer
======

Rendering Nette forms for Bootstrap 3
Based on [http://github.com/kdyby/BootstrapFormRenderer](http://github.com/kdyby/BootstrapFormRenderer)

Requirements
------------

- PHP 5.4 or higher

- [Nette Framework](https://github.com/nette/nette) 2.1 (version 2.2 for custom latte macros)



Installation
------------

The best way to install Instante/Bootstap3Renderer is using  [Composer](http://getcomposer.org/):

```sh
$ composer require instante/bootstrap-3-renderer:@dev
```